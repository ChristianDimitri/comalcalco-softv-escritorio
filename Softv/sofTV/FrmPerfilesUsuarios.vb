﻿Public Class FrmPerfilesUsuarios
    Dim IDMENU As Integer
    Dim CLVTIPOUSU As Integer
    Dim IDBOTON As Integer
    Private Sub FrmPerfilesUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UspLlenaOpcionesAConfigurar()
        UspLlenatipoUsuario()
        UspLlenaArbolAccesoMenuTMP()
        UspMuestraAccesosNoVisibles()
        UspMuestraAccesosVisibles()
    End Sub
    Private Sub UspLlenaOpcionesAConfigurar()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            DT = BaseII.ConsultaDT("UspLlenaOpcionesAConfigurar")
            If DT.Rows.Count > 0 Then
                ComboBox1.DataSource = DT
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenatipoUsuario()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            DT = BaseII.ConsultaDT("UspLlenatipoUsuario")
            If DT.Rows.Count > 0 Then
                ComboBox2.DataSource = DT
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspLlenaArbolAccesoMenuTMP()
        Try
            BaseII.limpiaParametros()
            BaseII.Inserta("UspLlenaArbolAccesoMenuTMP")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedValue = 1 Then
            Me.Panel1.Visible = False
            UspLlenaArbolAccesoMenuTMP()
            UspMuestraAccesosNoVisibles()
            UspMuestraAccesosVisibles()
        Else
            Me.Panel1.Visible = True
            UspMuestraFormulariosUsuario()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        CLVTIPOUSU = Me.ComboBox2.SelectedValue
        UspMuestraAccesosNoVisibles()
        UspMuestraAccesosVisibles()
        UspMuestraFormulariosUsuario()
    End Sub

    Private Sub UspMuestraAccesosNoVisibles()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTIPOUSU", SqlDbType.Int, CLVTIPOUSU)
            Me.ListBox1.DataSource = BaseII.ConsultaDT("UspMuestraAccesosNoVisibles")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraAccesosVisibles()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTIPOUSU", SqlDbType.Int, CLVTIPOUSU)
            Me.ListBox2.DataSource = BaseII.ConsultaDT("UspMuestraAccesosVisibles")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspSeleccionaUnMenuAcceso()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDMENU", SqlDbType.Int, IDMENU)
            BaseII.CreateMyParameter("@CLVTIPOUSU", SqlDbType.Int, CLVTIPOUSU)
            BaseII.Inserta("UspSeleccionaUnMenuAcceso")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspDeseleccionaUnMenuAcceso()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@IDMENU", SqlDbType.Int, IDMENU)
            BaseII.CreateMyParameter("@CLVTIPOUSU", SqlDbType.Int, CLVTIPOUSU)
            BaseII.Inserta("UspDeseleccionaUnMenuAcceso")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspSeleccionaTodoMenuAcceso()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTIPOUSU", SqlDbType.Int, CLVTIPOUSU)
            BaseII.Inserta("UspSeleccionaTodoMenuAcceso")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub UspDeseleccionaTodoMenuAcceso()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTIPOUSU", SqlDbType.Int, CLVTIPOUSU)
            BaseII.Inserta("UspDeseleccionaTodoMenuAcceso")
        Catch ex As Exception
            Throw ex
        End Try

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        IDMENU = CInt(Me.ListBox1.SelectedValue)
        UspSeleccionaUnMenuAcceso()
        UspMuestraAccesosNoVisibles()
        UspMuestraAccesosVisibles()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        IDMENU = CInt(Me.ListBox2.SelectedValue)
        UspDeseleccionaUnMenuAcceso()
        UspMuestraAccesosNoVisibles()
        UspMuestraAccesosVisibles()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        UspSeleccionaTodoMenuAcceso()
        UspMuestraAccesosVisibles()
        UspMuestraAccesosNoVisibles()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        UspDeseleccionaTodoMenuAcceso()
        UspMuestraAccesosNoVisibles()
        UspMuestraAccesosVisibles()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub UspMuestraBotonesUsuario()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVUSUARIO", SqlDbType.Int, CLVTIPOUSU)
            BaseII.CreateMyParameter("@IDFORMULARIO", SqlDbType.Int, CInt(Me.ListBox3.SelectedValue))
            DataGridView1.DataSource = BaseII.ConsultaDT("UspMuestraBotonesUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspMuestraFormulariosUsuario()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CLVTIPOUSUARIO", SqlDbType.Int, CLVTIPOUSU)
            Me.ListBox3.DataSource = BaseII.ConsultaDT("UspMuestraFormulariosUsuario")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaInformacionBotonesUsuarios()
        Try
            Dim I As Integer
            Dim VISIBLEFORM As Integer
            Dim VISIBLEBOTON As Integer
            For I = 0 To DataGridView1.Rows.Count - 1

                If Me.DataGridView1(2, I).Value.ToString = True Then
                    VISIBLEBOTON = 1
                Else
                    VISIBLEBOTON = 0
                End If

                IDBOTON = CInt(Me.DataGridView1(0, I).Value.ToString)

                BaseII.limpiaParametros()
                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@IDBOTON", SqlDbType.Int, IDBOTON)
                BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, CLVTIPOUSU)
                BaseII.CreateMyParameter("@ACTIVO", SqlDbType.Int, VISIBLEBOTON)
                BaseII.Inserta("UspGuardaInformacionBotonesUsuarios")

            Next


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ListBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBox3.SelectedIndexChanged
        UspMuestraBotonesUsuario()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        UspGuardaInformacionBotonesUsuarios()
        MsgBox("Fue guardado con exito", MsgBoxStyle.Information, "Perfiles")
    End Sub
End Class