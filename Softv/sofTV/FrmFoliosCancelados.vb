﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmFoliosCancelados

    Private Sub FrmFoliosCancelados2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LenaVendedor()
    End Sub

    Private Sub LenaVendedor()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MUESTRAVENDEDORES_2 ")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            Me.cmbVendedor.DataSource = dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        'Me.cmbVendedor.Text = ""
    End Sub

    Private Sub LlenaSerie()
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Dim strSQL As StringBuilder
        Dim conexion As New SqlConnection(MiConexion)
        If IsNumeric(Me.cmbVendedor.SelectedValue) = True Then
            'And Len(Trim(Me.Vendedor.SelectedText)) > 0 
            strSQL = New StringBuilder("EXEC Ultimo_SERIEYFOLIO " & CStr(Me.cmbVendedor.SelectedValue))
        Else
            strSQL = New StringBuilder("EXEC Ultimo_SERIEYFOLIO " & CStr(0))
        End If
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try
            dataAdapter.Fill(dataTable)
            Me.cmbSerie.DataSource = dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
        'Me.cmbSerie.Text = ""
    End Sub

    Private Sub LlenaFolio()
        Dim dataTable As New DataTable
        Dim bindingsource As New BindingSource
        Dim conexion As New SqlConnection(MiConexion)
        If IsNumeric(Me.cmbVendedor.SelectedValue) = True And Len(Me.cmbSerie.SelectedValue) > 0 Then
            Dim strSQL As New StringBuilder("EXEC Folio_Disponible " & CStr(Me.cmbVendedor.SelectedValue) & ", '" & CStr(Me.cmbSerie.SelectedValue) & "'")
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

            Try
                dataAdapter.Fill(dataTable)
                Me.cmbFolio.DataSource = dataTable
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
        'Me.cmbFolio.Text = ""
    End Sub

    Private Sub cancelaFolios()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Cancela_Folios", conexion)
        comando.CommandType = CommandType.StoredProcedure
        If IsNumeric(Me.cmbVendedor.SelectedValue) = True Then
            If Len(Me.cmbSerie.SelectedValue) > 0 Then
                If IsNumeric(Me.cmbFolio.SelectedValue) = True Then

                    Dim parametro1 As New SqlParameter("@Vendedor", SqlDbType.Int)
                    parametro1.Direction = ParameterDirection.Input
                    parametro1.Value = CInt(Me.cmbVendedor.SelectedValue)
                    comando.Parameters.Add(parametro1)

                    Dim parametro2 As New SqlParameter("@Serie", SqlDbType.VarChar)
                    parametro2.Direction = ParameterDirection.Input
                    parametro2.Value = CStr(Me.cmbSerie.SelectedValue)
                    comando.Parameters.Add(parametro2)

                    Dim parametro3 As New SqlParameter("@Folio", SqlDbType.VarChar)
                    parametro3.Direction = ParameterDirection.Input
                    parametro3.Value = CInt(Me.cmbFolio.SelectedValue)
                    comando.Parameters.Add(parametro3)

                    Try
                        conexion.Open()
                        comando.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Exclamation)
                    Finally
                        conexion.Close()
                        conexion.Dispose()
                        MsgBox("Folio Cancelado Correctamente!", MsgBoxStyle.Information)
                    End Try
                Else
                    MsgBox("Seleccione al menos un Folio", MsgBoxStyle.Information)
                End If
            Else
                MsgBox("Seleccione al menos una Serie", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione al menos un Vendedor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub cmbVendedor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVendedor.TextChanged
        LlenaSerie()
    End Sub

    Private Sub cmbSerie_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSerie.TextChanged
        LlenaFolio()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.CLOSE()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        cancelaFolios()
        LlenaFolio()
    End Sub

  
End Class