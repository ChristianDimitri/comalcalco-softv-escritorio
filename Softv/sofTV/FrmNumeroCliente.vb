﻿Public Class FrmNumeroCliente

    Private Sub FrmNumeroCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If OpcionCli = "C" Then
            TextBoxSuperManzana.Enabled = False
            TextBoxManzana.Enabled = False
            TextBoxLote.Enabled = False
            TextBoxNum.Enabled = False
            Button5.Enabled = False
        End If

        If OpcionCli = "C" Or OpcionCli = "M" Then
            DameNumeroClienteDetalle()
        End If
    End Sub

    Private Sub DameNumeroClienteDetalle()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.BigInt, Contrato)
        BaseII.CreateMyParameter("@Supermanzana", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@Manzana", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@Lote", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.CreateMyParameter("@Numero", ParameterDirection.Output, SqlDbType.VarChar, 50)
        BaseII.ProcedimientoOutPut("DameNumeroClienteDetalle")
        TextBoxSuperManzana.Text = BaseII.dicoPar("@Supermanzana").ToString()
        TextBoxManzana.Text = BaseII.dicoPar("@Manzana").ToString()
        TextBoxLote.Text = BaseII.dicoPar("@Lote").ToString()
        TextBoxNum.Text = BaseII.dicoPar("@Numero").ToString()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim cadAux As String = ""

        If TextBoxSuperManzana.Text.Length > 0 Then
            cadAux = cadAux + "SM" + TextBoxSuperManzana.Text
        End If
        If TextBoxManzana.Text.Length > 0 Then
            cadAux = cadAux + "-M" + TextBoxManzana.Text
        End If
        If TextBoxLote.Text.Length > 0 Then
            cadAux = cadAux + "-L" + TextBoxLote.Text
        End If
        If TextBoxNum.Text.Length > 0 Then
            cadAux = cadAux + "-N" + TextBoxNum.Text
        End If
        SupermanzanaAux = TextBoxSuperManzana.Text
        ManzanaAux = TextBoxManzana.Text
        LoteAux = TextBoxLote.Text
        NumeroAux = TextBoxNum.Text
        ActivaGuardaNumeroCliente = True

        FrmClientes.NUMEROTextBox.Text = cadAux
        Me.Close()
    End Sub
End Class