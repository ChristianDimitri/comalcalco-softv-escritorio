﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNumeroCliente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label1 As System.Windows.Forms.Label
        Dim ENTRECALLESLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Me.TextBoxSuperManzana = New System.Windows.Forms.TextBox()
        Me.TextBoxManzana = New System.Windows.Forms.TextBox()
        Me.TextBoxLote = New System.Windows.Forms.TextBox()
        Me.TextBoxNum = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Label1 = New System.Windows.Forms.Label()
        ENTRECALLESLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Label1.Location = New System.Drawing.Point(26, 21)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(108, 15)
        Label1.TabIndex = 50
        Label1.Text = "Supermanzana:"
        '
        'TextBoxSuperManzana
        '
        Me.TextBoxSuperManzana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxSuperManzana.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxSuperManzana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxSuperManzana.Location = New System.Drawing.Point(136, 21)
        Me.TextBoxSuperManzana.MaxLength = 250
        Me.TextBoxSuperManzana.Name = "TextBoxSuperManzana"
        Me.TextBoxSuperManzana.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBoxSuperManzana.Size = New System.Drawing.Size(120, 22)
        Me.TextBoxSuperManzana.TabIndex = 49
        '
        'ENTRECALLESLabel
        '
        ENTRECALLESLabel.AutoSize = True
        ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ENTRECALLESLabel.Location = New System.Drawing.Point(26, 54)
        ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        ENTRECALLESLabel.Size = New System.Drawing.Size(70, 15)
        ENTRECALLESLabel.TabIndex = 52
        ENTRECALLESLabel.Text = "Manzana:"
        '
        'TextBoxManzana
        '
        Me.TextBoxManzana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxManzana.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxManzana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxManzana.Location = New System.Drawing.Point(136, 54)
        Me.TextBoxManzana.MaxLength = 250
        Me.TextBoxManzana.Name = "TextBoxManzana"
        Me.TextBoxManzana.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBoxManzana.Size = New System.Drawing.Size(120, 22)
        Me.TextBoxManzana.TabIndex = 51
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Label2.Location = New System.Drawing.Point(26, 86)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(39, 15)
        Label2.TabIndex = 54
        Label2.Text = "Lote:"
        '
        'TextBoxLote
        '
        Me.TextBoxLote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxLote.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxLote.Location = New System.Drawing.Point(136, 86)
        Me.TextBoxLote.MaxLength = 250
        Me.TextBoxLote.Name = "TextBoxLote"
        Me.TextBoxLote.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBoxLote.Size = New System.Drawing.Size(120, 22)
        Me.TextBoxLote.TabIndex = 53
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(26, 119)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(62, 15)
        Label3.TabIndex = 56
        Label3.Text = "Número:"
        '
        'TextBoxNum
        '
        Me.TextBoxNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxNum.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBoxNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxNum.Location = New System.Drawing.Point(136, 119)
        Me.TextBoxNum.MaxLength = 250
        Me.TextBoxNum.Name = "TextBoxNum"
        Me.TextBoxNum.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TextBoxNum.Size = New System.Drawing.Size(120, 22)
        Me.TextBoxNum.TabIndex = 55
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(74, 161)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 57
        Me.Button5.Text = "Guardar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'FrmNumeroCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(282, 216)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.TextBoxNum)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.TextBoxLote)
        Me.Controls.Add(ENTRECALLESLabel)
        Me.Controls.Add(Me.TextBoxManzana)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.TextBoxSuperManzana)
        Me.Name = "FrmNumeroCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Número de Cliente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBoxSuperManzana As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxManzana As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxLote As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxNum As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
