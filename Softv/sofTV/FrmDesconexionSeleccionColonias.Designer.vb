﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDesconexionSeleccionColonias
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvIzq = New System.Windows.Forms.DataGridView()
        Me.dgvDer = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.bnGenerar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lbMensaje = New System.Windows.Forms.Label()
        Me.CLV_COLONIAIZQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBREIZQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_Ciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreCd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLV_COLONIAR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBREDER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clv_CiudadR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreCdR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvIzq, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvIzq
        '
        Me.dgvIzq.AllowUserToAddRows = False
        Me.dgvIzq.AllowUserToDeleteRows = False
        Me.dgvIzq.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIzq.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvIzq.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIzq.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLV_COLONIAIZQ, Me.NOMBREIZQ, Me.Clv_Ciudad, Me.NombreCd})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIzq.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvIzq.Location = New System.Drawing.Point(24, 47)
        Me.dgvIzq.Name = "dgvIzq"
        Me.dgvIzq.ReadOnly = True
        Me.dgvIzq.RowHeadersVisible = False
        Me.dgvIzq.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIzq.Size = New System.Drawing.Size(403, 313)
        Me.dgvIzq.TabIndex = 0
        '
        'dgvDer
        '
        Me.dgvDer.AllowUserToAddRows = False
        Me.dgvDer.AllowUserToDeleteRows = False
        Me.dgvDer.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDer.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLV_COLONIAR, Me.NOMBREDER, Me.Clv_CiudadR, Me.NombreCdR})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDer.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDer.Location = New System.Drawing.Point(522, 47)
        Me.dgvDer.Name = "dgvDer"
        Me.dgvDer.ReadOnly = True
        Me.dgvDer.RowHeadersVisible = False
        Me.dgvDer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDer.Size = New System.Drawing.Size(403, 313)
        Me.dgvDer.TabIndex = 1
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(438, 120)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(438, 149)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(438, 248)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(438, 277)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'bnGenerar
        '
        Me.bnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGenerar.Location = New System.Drawing.Point(647, 385)
        Me.bnGenerar.Name = "bnGenerar"
        Me.bnGenerar.Size = New System.Drawing.Size(136, 36)
        Me.bnGenerar.TabIndex = 6
        Me.bnGenerar.Text = "&GENERAR"
        Me.bnGenerar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(789, 385)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 7
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(21, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(164, 15)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Colonias sin seleccionar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(519, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(159, 15)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Colonias seleccionadas"
        '
        'lbMensaje
        '
        Me.lbMensaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbMensaje.Location = New System.Drawing.Point(24, 363)
        Me.lbMensaje.Name = "lbMensaje"
        Me.lbMensaje.Size = New System.Drawing.Size(358, 71)
        Me.lbMensaje.TabIndex = 10
        '
        'CLV_COLONIAIZQ
        '
        Me.CLV_COLONIAIZQ.DataPropertyName = "CLV_COLONIA"
        Me.CLV_COLONIAIZQ.HeaderText = "CLV_COLONIA"
        Me.CLV_COLONIAIZQ.Name = "CLV_COLONIAIZQ"
        Me.CLV_COLONIAIZQ.ReadOnly = True
        Me.CLV_COLONIAIZQ.Visible = False
        '
        'NOMBREIZQ
        '
        Me.NOMBREIZQ.DataPropertyName = "NOMBRE"
        Me.NOMBREIZQ.HeaderText = "Colonias"
        Me.NOMBREIZQ.Name = "NOMBREIZQ"
        Me.NOMBREIZQ.ReadOnly = True
        Me.NOMBREIZQ.Width = 200
        '
        'Clv_Ciudad
        '
        Me.Clv_Ciudad.DataPropertyName = "Clv_Ciudad"
        Me.Clv_Ciudad.HeaderText = "Clv_Ciudad"
        Me.Clv_Ciudad.Name = "Clv_Ciudad"
        Me.Clv_Ciudad.ReadOnly = True
        Me.Clv_Ciudad.Visible = False
        '
        'NombreCd
        '
        Me.NombreCd.DataPropertyName = "NombreCd"
        Me.NombreCd.HeaderText = "Ciudades"
        Me.NombreCd.Name = "NombreCd"
        Me.NombreCd.ReadOnly = True
        Me.NombreCd.Width = 200
        '
        'CLV_COLONIAR
        '
        Me.CLV_COLONIAR.DataPropertyName = "CLV_COLONIA"
        Me.CLV_COLONIAR.HeaderText = "CLV_COLONIA"
        Me.CLV_COLONIAR.Name = "CLV_COLONIAR"
        Me.CLV_COLONIAR.ReadOnly = True
        Me.CLV_COLONIAR.Visible = False
        '
        'NOMBREDER
        '
        Me.NOMBREDER.DataPropertyName = "NOMBRE"
        Me.NOMBREDER.HeaderText = "Colonias"
        Me.NOMBREDER.Name = "NOMBREDER"
        Me.NOMBREDER.ReadOnly = True
        Me.NOMBREDER.Width = 200
        '
        'Clv_CiudadR
        '
        Me.Clv_CiudadR.DataPropertyName = "Clv_Ciudad"
        Me.Clv_CiudadR.HeaderText = "Clv_Ciudad"
        Me.Clv_CiudadR.Name = "Clv_CiudadR"
        Me.Clv_CiudadR.ReadOnly = True
        Me.Clv_CiudadR.Visible = False
        '
        'NombreCdR
        '
        Me.NombreCdR.DataPropertyName = "NombreCd"
        Me.NombreCdR.HeaderText = "Ciudades"
        Me.NombreCdR.Name = "NombreCdR"
        Me.NombreCdR.ReadOnly = True
        Me.NombreCdR.Width = 200
        '
        'FrmDesconexionSeleccionColonias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(943, 446)
        Me.Controls.Add(Me.lbMensaje)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnGenerar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dgvDer)
        Me.Controls.Add(Me.dgvIzq)
        Me.Name = "FrmDesconexionSeleccionColonias"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Colonias"
        CType(Me.dgvIzq, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvIzq As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDer As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents bnGenerar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lbMensaje As System.Windows.Forms.Label
    Friend WithEvents CLV_COLONIAIZQ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBREIZQ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Ciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreCd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLV_COLONIAR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBREDER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_CiudadR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreCdR As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
