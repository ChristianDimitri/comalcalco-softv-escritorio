﻿Imports System.Data.SqlClient
Public Class FrmBancos
    Private clv_txt As String = Nothing
    Private nombrebanco As String = Nothing

    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                clv_txt = Me.Clv_TxtTextBox.Text
                nombrebanco = Me.NombreTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardadatosbitacora(ByVal op As Integer)
        Try
            Select Case op
                Case 0
                    If opcion = "M" Then
                        'clv_txt = Me.Clv_TxtTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.Clv_TxtTextBox.Name, clv_txt, Me.Clv_TxtTextBox.Text, LocClv_Ciudad)
                        'nombrebanco = Me.NombreTextBox.Text
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name, nombrebanco, Me.NombreTextBox.Text, LocClv_Ciudad)
                    ElseIf opcion = "N" Then
                        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Agregó Un Banco", "", "Se Agregó Un Banco: " + Me.NombreTextBox.Text, LocClv_Ciudad)
                    End If
                Case 1
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Un Banco", "", "Se Elimino Un Banco: " + Me.NombreTextBox.Text, LocClv_Ciudad)
            End Select
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub BUSCA(ByVal CLAVE As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONBANCOSTableAdapter.Connection = CON
            Me.CONBANCOSTableAdapter.Fill(Me.NewSofTvDataSet.CONBANCOS, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmBancos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONBANCOSBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
        End If
        If opcion = "N" Or opcion = "M" Then
            UspDesactivaBotones(Me, Me.Name)
        End If
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
    End Sub

    Private Sub CONBANCOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONBANCOSBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONBANCOSBindingSource.EndEdit()
        Me.CONBANCOSTableAdapter.Connection = CON
        Me.CONBANCOSTableAdapter.Update(Me.NewSofTvDataSet.CONBANCOS)
        guardadatosbitacora(0)
        MsgBox(mensaje5)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONBANCOSTableAdapter.Connection = CON
        Me.CONBANCOSTableAdapter.Delete(gloClave)
        guardadatosbitacora(1)
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONBANCOSBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub ClaveTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaveTextBox.TextChanged
        gloClave = Me.ClaveTextBox.Text
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub NombreTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NombreTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NombreTextBox, Asc(LCase(e.KeyChar)), "S")))
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub
End Class