<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCONEX
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim Clv_OrdenLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim ExtAdicLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCONEX))
        Me.CONCONEXBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONCONEXBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.CONCONEXBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox
        Me.ContratoTextBox = New System.Windows.Forms.TextBox
        Me.ExtAdicTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.CONCONEXTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONCONEXTableAdapter
        Me.BORDetOrdSer_INTELIGENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORDetOrdSer_INTELIGENTETableAdapter = New sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
        ClaveLabel = New System.Windows.Forms.Label
        Clv_OrdenLabel = New System.Windows.Forms.Label
        ContratoLabel = New System.Windows.Forms.Label
        ExtAdicLabel = New System.Windows.Forms.Label
        CType(Me.CONCONEXBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCONEXBindingNavigator.SuspendLayout()
        CType(Me.CONCONEXBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Location = New System.Drawing.Point(19, 3)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(37, 13)
        ClaveLabel.TabIndex = 2
        ClaveLabel.Text = "Clave:"
        '
        'Clv_OrdenLabel
        '
        Clv_OrdenLabel.AutoSize = True
        Clv_OrdenLabel.Location = New System.Drawing.Point(19, 3)
        Clv_OrdenLabel.Name = "Clv_OrdenLabel"
        Clv_OrdenLabel.Size = New System.Drawing.Size(57, 13)
        Clv_OrdenLabel.TabIndex = 4
        Clv_OrdenLabel.Text = "Clv Orden:"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Location = New System.Drawing.Point(19, 3)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(50, 13)
        ContratoLabel.TabIndex = 6
        ContratoLabel.Text = "Contrato:"
        '
        'ExtAdicLabel
        '
        ExtAdicLabel.AutoSize = True
        ExtAdicLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ExtAdicLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ExtAdicLabel.Location = New System.Drawing.Point(30, 72)
        ExtAdicLabel.Name = "ExtAdicLabel"
        ExtAdicLabel.Size = New System.Drawing.Size(241, 15)
        ExtAdicLabel.TabIndex = 8
        ExtAdicLabel.Text = "Numero de Extensiones por Instalar:"
        '
        'CONCONEXBindingNavigator
        '
        Me.CONCONEXBindingNavigator.AddNewItem = Nothing
        Me.CONCONEXBindingNavigator.BindingSource = Me.CONCONEXBindingSource
        Me.CONCONEXBindingNavigator.CountItem = Nothing
        Me.CONCONEXBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCONEXBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONCONEXBindingNavigatorSaveItem})
        Me.CONCONEXBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCONEXBindingNavigator.MoveFirstItem = Nothing
        Me.CONCONEXBindingNavigator.MoveLastItem = Nothing
        Me.CONCONEXBindingNavigator.MoveNextItem = Nothing
        Me.CONCONEXBindingNavigator.MovePreviousItem = Nothing
        Me.CONCONEXBindingNavigator.Name = "CONCONEXBindingNavigator"
        Me.CONCONEXBindingNavigator.PositionItem = Nothing
        Me.CONCONEXBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONCONEXBindingNavigator.Size = New System.Drawing.Size(434, 25)
        Me.CONCONEXBindingNavigator.TabIndex = 1
        Me.CONCONEXBindingNavigator.TabStop = True
        Me.CONCONEXBindingNavigator.Text = "BindingNavigator1"
        '
        'CONCONEXBindingSource
        '
        Me.CONCONEXBindingSource.DataMember = "CONCONEX"
        Me.CONCONEXBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(82, 22)
        Me.BindingNavigatorDeleteItem.Text = "&BORRAR"
        '
        'CONCONEXBindingNavigatorSaveItem
        '
        Me.CONCONEXBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCONEXBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCONEXBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCONEXBindingNavigatorSaveItem.Name = "CONCONEXBindingNavigatorSaveItem"
        Me.CONCONEXBindingNavigatorSaveItem.Size = New System.Drawing.Size(87, 22)
        Me.CONCONEXBindingNavigatorSaveItem.Text = "&ACEPTAR"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCONEXBindingSource, "Clave", True))
        Me.ClaveTextBox.Location = New System.Drawing.Point(82, 0)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ClaveTextBox.TabIndex = 3
        Me.ClaveTextBox.TabStop = False
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCONEXBindingSource, "Clv_Orden", True))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(82, 0)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_OrdenTextBox.TabIndex = 5
        Me.Clv_OrdenTextBox.TabStop = False
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCONEXBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(82, 0)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContratoTextBox.TabIndex = 7
        Me.ContratoTextBox.TabStop = False
        '
        'ExtAdicTextBox
        '
        Me.ExtAdicTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCONEXBindingSource, "ExtAdic", True))
        Me.ExtAdicTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExtAdicTextBox.Location = New System.Drawing.Point(277, 69)
        Me.ExtAdicTextBox.MaxLength = 3
        Me.ExtAdicTextBox.Name = "ExtAdicTextBox"
        Me.ExtAdicTextBox.Size = New System.Drawing.Size(100, 22)
        Me.ExtAdicTextBox.TabIndex = 0
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(286, 177)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONCONEXTableAdapter
        '
        Me.CONCONEXTableAdapter.ClearBeforeFill = True
        '
        'BORDetOrdSer_INTELIGENTEBindingSource
        '
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataMember = "BORDetOrdSer_INTELIGENTE"
        Me.BORDetOrdSer_INTELIGENTEBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'BORDetOrdSer_INTELIGENTETableAdapter
        '
        Me.BORDetOrdSer_INTELIGENTETableAdapter.ClearBeforeFill = True
        '
        'FrmCONEX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(434, 222)
        Me.Controls.Add(Me.CONCONEXBindingNavigator)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(ClaveLabel)
        Me.Controls.Add(Me.ClaveTextBox)
        Me.Controls.Add(Clv_OrdenLabel)
        Me.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Controls.Add(ContratoLabel)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.Controls.Add(ExtAdicLabel)
        Me.Controls.Add(Me.ExtAdicTextBox)
        Me.Name = "FrmCONEX"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Extensiones por Instalar"
        Me.TopMost = True
        CType(Me.CONCONEXBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCONEXBindingNavigator.ResumeLayout(False)
        Me.CONCONEXBindingNavigator.PerformLayout()
        CType(Me.CONCONEXBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORDetOrdSer_INTELIGENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONCONEXBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCONEXTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONCONEXTableAdapter
    Friend WithEvents CONCONEXBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCONEXBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ExtAdicTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BORDetOrdSer_INTELIGENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORDetOrdSer_INTELIGENTETableAdapter As sofTV.NewSofTvDataSetTableAdapters.BORDetOrdSer_INTELIGENTETableAdapter
End Class
