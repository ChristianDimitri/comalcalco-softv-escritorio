Imports System.Data.SqlClient
Public Class BRWMotivoCancelacion

    Private Sub BRWMotivoCancelacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'Me.DamePermisosFormTableAdapter.Fill(Me.NewSofTvDataSet.DamePermisosForm, GloTipoUsuario, Me.Name, 1, glolec, gloescr, gloctr)
        If gloescr = 1 Then
            Me.Button2.Enabled = False
            Me.Button4.Enabled = False
        End If
        llenacalles()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub



    Private Sub llenacalles()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.BuscaMotivoCancelacionTableAdapter.Connection = CON
            Me.BuscaMotivoCancelacionTableAdapter.Fill(Me.NewSofTvDataSet.BuscaMotivoCancelacion, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(3, Integer)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub


    Private Sub BUSCA_CLAVE()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.TextBox1.Text) = True Then
                Me.BuscaMotivoCancelacionTableAdapter.Connection = CON
                Me.BuscaMotivoCancelacionTableAdapter.Fill(Me.NewSofTvDataSet.BuscaMotivoCancelacion, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)))
            Else
                MsgBox("La Clave que busca no es valida")
            End If
            Me.TextBox1.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub BUSCA_NOMBRE()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If Len(Trim(Me.TextBox2.Text)) > 0 Then
                Me.BuscaMotivoCancelacionTableAdapter.Connection = CON
                Me.BuscaMotivoCancelacionTableAdapter.Fill(Me.NewSofTvDataSet.BuscaMotivoCancelacion, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)))
            Else
                MsgBox("El Nombre de la Calle que busca no es valida")
            End If
            Me.TextBox2.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BUSCA_CLAVE()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCA_NOMBRE()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCA_CLAVE()
        End If
    End Sub



    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCA_NOMBRE()
        End If
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        gloClave = 0
        FrmMotivosCancelacion.Show()
    End Sub

    Private Sub consultar()
        opcion = "C"
        gloClave = Me.Clv_calleLabel2.Text
        FrmMotivosCancelacion.Show()
    End Sub

    Private Sub Modificar()
        opcion = "M"
        gloClave = Me.Clv_calleLabel2.Text
        FrmMotivosCancelacion.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            consultar()
        Else
            MsgBox(mensaje2)
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.DataGridView1.RowCount > 0 Then
            Me.BUSCAclavesTableAdapter.Connection = CON
            Me.BUSCAclavesTableAdapter.Fill(Me.DataSetLidia.BUSCAclaves, Me.Clv_calleLabel2.Text, Me.CMBNombreTextBox.Text)
            If Me.TextBox3.Text <> "" And Me.TextBox4.Text <> "" Then
                MsgBox("No Se Puede Modificar el Registro, Ya Que es Fijo", MsgBoxStyle.Information, "Atenci�n")
            Else
                Modificar()
            End If
        Else
            MsgBox(mensaje1)
        End If
        CON.Close()
    End Sub


    Private Sub BrwCalles_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            llenacalles()
        End If
    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Button3.Enabled = True Then
            consultar()
        ElseIf Button4.Enabled = True Then
            Modificar()
        End If
    End Sub


End Class