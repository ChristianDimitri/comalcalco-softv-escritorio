﻿Public Class FrmDetalleMininodo
    Private Sub FrmDetMininodo_Load(sender As Object, e As EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Llena_Aparatos()
        muestra_Datos()
    End Sub


    Private Sub Llena_Aparatos()
        BaseII.limpiaParametros()
        CMBoxStatus.DataSource = BaseII.ConsultaDT("SP_StatusCableModem")
        CMBoxStatus.DisplayMember = "Concepto"
        CMBoxStatus.ValueMember = "Clv_StatusCableModem"
    End Sub

    Private Sub muestra_Datos()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@contrato", SqlDbType.Int, GloContrato)
        BaseII.CreateMyParameter("@SerieAparato", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.CreateMyParameter("@status", ParameterDirection.Output, SqlDbType.VarChar, 1)
        BaseII.CreateMyParameter("@observaciones", ParameterDirection.Output, SqlDbType.VarChar, 100)
        BaseII.CreateMyParameter("@F_activacion", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.CreateMyParameter("@F_baja", ParameterDirection.Output, SqlDbType.VarChar, 20)
        BaseII.ProcedimientoOutPut("sp_detMininodo")
        LblMac.Text = BaseII.dicoPar("@SerieAparato").ToString()
        CMBoxStatus.SelectedValue = BaseII.dicoPar("@status").ToString()
        TBObs.Text = BaseII.dicoPar("@observaciones").ToString()
        TBInstalacion.Text = BaseII.dicoPar("@F_activacion").ToString()
        TBBaja.Text = BaseII.dicoPar("@F_baja").ToString()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
End Class