<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmModificaCnrPPE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Mac_addressLabel As System.Windows.Forms.Label
        Dim Fecha_habilitarLabel As System.Windows.Forms.Label
        Dim ResultadoLabel As System.Windows.Forms.Label
        Dim PaqueteLabel As System.Windows.Forms.Label
        Dim ComandoLabel As System.Windows.Forms.Label
        Dim Numero_de_contratoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmModificaCnrPPE))
        Me.dtpFechaH = New System.Windows.Forms.DateTimePicker
        Me.lblResultado = New System.Windows.Forms.TextBox
        Me.lblComando = New System.Windows.Forms.TextBox
        Me.cbResultado = New System.Windows.Forms.ComboBox
        Me.cbComando = New System.Windows.Forms.ComboBox
        Me.txtPaquete = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtMac = New System.Windows.Forms.TextBox
        Me.txtContrato = New System.Windows.Forms.TextBox
        Me.btnSalir = New System.Windows.Forms.Button
        Me.ConCNRDigBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ConCNRDigBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Mac_addressLabel = New System.Windows.Forms.Label
        Fecha_habilitarLabel = New System.Windows.Forms.Label
        ResultadoLabel = New System.Windows.Forms.Label
        PaqueteLabel = New System.Windows.Forms.Label
        ComandoLabel = New System.Windows.Forms.Label
        Numero_de_contratoLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.ConCNRDigBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ConCNRDigBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Mac_addressLabel
        '
        Mac_addressLabel.AutoSize = True
        Mac_addressLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Mac_addressLabel.Location = New System.Drawing.Point(71, 60)
        Mac_addressLabel.Name = "Mac_addressLabel"
        Mac_addressLabel.Size = New System.Drawing.Size(103, 16)
        Mac_addressLabel.TabIndex = 8
        Mac_addressLabel.Text = "Mac Address:"
        '
        'Fecha_habilitarLabel
        '
        Fecha_habilitarLabel.AutoSize = True
        Fecha_habilitarLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_habilitarLabel.Location = New System.Drawing.Point(20, 233)
        Fecha_habilitarLabel.Name = "Fecha_habilitarLabel"
        Fecha_habilitarLabel.Size = New System.Drawing.Size(154, 16)
        Fecha_habilitarLabel.TabIndex = 22
        Fecha_habilitarLabel.Text = "Fecha para Habilitar:"
        '
        'ResultadoLabel
        '
        ResultadoLabel.AutoSize = True
        ResultadoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ResultadoLabel.Location = New System.Drawing.Point(91, 187)
        ResultadoLabel.Name = "ResultadoLabel"
        ResultadoLabel.Size = New System.Drawing.Size(83, 16)
        ResultadoLabel.TabIndex = 14
        ResultadoLabel.Text = "Resultado:"
        '
        'PaqueteLabel
        '
        PaqueteLabel.AutoSize = True
        PaqueteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PaqueteLabel.Location = New System.Drawing.Point(104, 99)
        PaqueteLabel.Name = "PaqueteLabel"
        PaqueteLabel.Size = New System.Drawing.Size(70, 16)
        PaqueteLabel.TabIndex = 10
        PaqueteLabel.Text = "Paquete:"
        '
        'ComandoLabel
        '
        ComandoLabel.AutoSize = True
        ComandoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ComandoLabel.Location = New System.Drawing.Point(96, 142)
        ComandoLabel.Name = "ComandoLabel"
        ComandoLabel.Size = New System.Drawing.Size(78, 16)
        ComandoLabel.TabIndex = 12
        ComandoLabel.Text = "Comando:"
        '
        'Numero_de_contratoLabel
        '
        Numero_de_contratoLabel.AutoSize = True
        Numero_de_contratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Numero_de_contratoLabel.Location = New System.Drawing.Point(24, 19)
        Numero_de_contratoLabel.Name = "Numero_de_contratoLabel"
        Numero_de_contratoLabel.Size = New System.Drawing.Size(150, 16)
        Numero_de_contratoLabel.TabIndex = 4
        Numero_de_contratoLabel.Text = "Número de Contrato:"
        '
        'dtpFechaH
        '
        Me.dtpFechaH.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaH.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaH.Location = New System.Drawing.Point(180, 227)
        Me.dtpFechaH.Name = "dtpFechaH"
        Me.dtpFechaH.Size = New System.Drawing.Size(110, 22)
        Me.dtpFechaH.TabIndex = 6
        '
        'lblResultado
        '
        Me.lblResultado.Location = New System.Drawing.Point(180, 183)
        Me.lblResultado.Name = "lblResultado"
        Me.lblResultado.ReadOnly = True
        Me.lblResultado.Size = New System.Drawing.Size(90, 20)
        Me.lblResultado.TabIndex = 27
        Me.lblResultado.TabStop = False
        '
        'lblComando
        '
        Me.lblComando.Location = New System.Drawing.Point(180, 138)
        Me.lblComando.Name = "lblComando"
        Me.lblComando.ReadOnly = True
        Me.lblComando.Size = New System.Drawing.Size(90, 20)
        Me.lblComando.TabIndex = 26
        Me.lblComando.TabStop = False
        '
        'cbResultado
        '
        Me.cbResultado.FormattingEnabled = True
        Me.cbResultado.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cbResultado.Items.AddRange(New Object() {"0", "1"})
        Me.cbResultado.Location = New System.Drawing.Point(284, 182)
        Me.cbResultado.Name = "cbResultado"
        Me.cbResultado.Size = New System.Drawing.Size(96, 21)
        Me.cbResultado.TabIndex = 5
        '
        'cbComando
        '
        Me.cbComando.FormattingEnabled = True
        Me.cbComando.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cbComando.Items.AddRange(New Object() {"SCENTL", "SDENTL", "SOFULL"})
        Me.cbComando.Location = New System.Drawing.Point(284, 137)
        Me.cbComando.Name = "cbComando"
        Me.cbComando.Size = New System.Drawing.Size(96, 21)
        Me.cbComando.TabIndex = 4
        '
        'txtPaquete
        '
        Me.txtPaquete.Enabled = False
        Me.txtPaquete.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaquete.Location = New System.Drawing.Point(180, 93)
        Me.txtPaquete.Name = "txtPaquete"
        Me.txtPaquete.Size = New System.Drawing.Size(200, 22)
        Me.txtPaquete.TabIndex = 2
        Me.txtPaquete.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Mac_addressLabel)
        Me.Panel1.Controls.Add(Me.dtpFechaH)
        Me.Panel1.Controls.Add(Me.lblResultado)
        Me.Panel1.Controls.Add(Fecha_habilitarLabel)
        Me.Panel1.Controls.Add(Me.lblComando)
        Me.Panel1.Controls.Add(ResultadoLabel)
        Me.Panel1.Controls.Add(PaqueteLabel)
        Me.Panel1.Controls.Add(Me.cbResultado)
        Me.Panel1.Controls.Add(ComandoLabel)
        Me.Panel1.Controls.Add(Me.cbComando)
        Me.Panel1.Controls.Add(Me.txtPaquete)
        Me.Panel1.Controls.Add(Me.txtMac)
        Me.Panel1.Controls.Add(Me.txtContrato)
        Me.Panel1.Controls.Add(Numero_de_contratoLabel)
        Me.Panel1.Location = New System.Drawing.Point(9, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(558, 264)
        Me.Panel1.TabIndex = 22
        Me.Panel1.TabStop = True
        '
        'txtMac
        '
        Me.txtMac.Enabled = False
        Me.txtMac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMac.Location = New System.Drawing.Point(180, 54)
        Me.txtMac.Name = "txtMac"
        Me.txtMac.Size = New System.Drawing.Size(200, 22)
        Me.txtMac.TabIndex = 1
        '
        'txtContrato
        '
        Me.txtContrato.Enabled = False
        Me.txtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.Location = New System.Drawing.Point(180, 13)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.Size = New System.Drawing.Size(200, 22)
        Me.txtContrato.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(444, 318)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 27
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ConCNRDigBindingNavigator
        '
        Me.ConCNRDigBindingNavigator.AddNewItem = Nothing
        Me.ConCNRDigBindingNavigator.CountItem = Nothing
        Me.ConCNRDigBindingNavigator.DeleteItem = Nothing
        Me.ConCNRDigBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConCNRDigBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConCNRDigBindingNavigatorSaveItem})
        Me.ConCNRDigBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.ConCNRDigBindingNavigator.MoveFirstItem = Nothing
        Me.ConCNRDigBindingNavigator.MoveLastItem = Nothing
        Me.ConCNRDigBindingNavigator.MoveNextItem = Nothing
        Me.ConCNRDigBindingNavigator.MovePreviousItem = Nothing
        Me.ConCNRDigBindingNavigator.Name = "ConCNRDigBindingNavigator"
        Me.ConCNRDigBindingNavigator.PositionItem = Nothing
        Me.ConCNRDigBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ConCNRDigBindingNavigator.Size = New System.Drawing.Size(592, 25)
        Me.ConCNRDigBindingNavigator.TabIndex = 34
        Me.ConCNRDigBindingNavigator.TabStop = True
        Me.ConCNRDigBindingNavigator.Text = "BindingNavigator1"
        '
        'ConCNRDigBindingNavigatorSaveItem
        '
        Me.ConCNRDigBindingNavigatorSaveItem.Image = CType(resources.GetObject("ConCNRDigBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.ConCNRDigBindingNavigatorSaveItem.Name = "ConCNRDigBindingNavigatorSaveItem"
        Me.ConCNRDigBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.ConCNRDigBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'FrmModificaCnrPPE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 366)
        Me.Controls.Add(Me.ConCNRDigBindingNavigator)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "FrmModificaCnrPPE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CNR PPE"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.ConCNRDigBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ConCNRDigBindingNavigator.ResumeLayout(False)
        Me.ConCNRDigBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpFechaH As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblResultado As System.Windows.Forms.TextBox
    Friend WithEvents lblComando As System.Windows.Forms.TextBox
    Friend WithEvents cbResultado As System.Windows.Forms.ComboBox
    Friend WithEvents cbComando As System.Windows.Forms.ComboBox
    Friend WithEvents txtPaquete As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtMac As System.Windows.Forms.TextBox
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ConCNRDigBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ConCNRDigBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
End Class
