Imports System.Data.SqlClient

Public Class FrmRefBan

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub FrmRefBan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Label3.ForeColor = Color.Black
        Checar()
        Busca()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Guarda()
    End Sub
    Private Sub Checar()
        If opcion = "C" Then
            Me.GroupBox1.Enabled = False
            Me.Button1.Enabled = False
        ElseIf opcion = "M" Then
            Me.GroupBox1.Enabled = True
            Me.Button1.Enabled = True
        End If
    End Sub

    Private Sub Busca()

        Dim con As New SqlConnection(MiConexion)
        Dim reader As SqlDataReader
        Dim cmd As New SqlCommand("CONSULTA_Referencias", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Contrato", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = Contrato
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            reader = cmd.ExecuteReader()
            Using reader
                While reader.Read
                    Me.TextBox2.Text = reader.GetValue(0)
                    Me.TextBox1.Text = reader.GetValue(1)
                End While
            End Using
            Me.TextBox3.Text = Contrato

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()


    End Sub

    Private Sub Guarda()
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("MODIFICA_Referencias", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Contrato", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = Contrato
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                 "@Referencia_Hsbc", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.TextBox2.Text
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
               "@Referencia_Santander", SqlDbType.VarChar, 50)
        prm.Direction = ParameterDirection.Input
        prm.Value = Me.TextBox1.Text
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            MsgBox("Se Guardo con �xito", MsgBoxStyle.Information, "Mensaje")

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()
        Me.Close()

    End Sub
End Class